var gulp = require('gulp');
var util = require('gulp-util');
var plumber = require('gulp-plumber');
var less = require('gulp-less');
var include = require("gulp-include");
var rename = require("gulp-rename");
var path = require('path');
var watch = require('gulp-watch');
var nunjucks = require('gulp-nunjucks');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var connect = require('gulp-connect');
var cssnano = require('gulp-cssnano');
var uglify = require('gulp-uglify');
var htmlmin = require('gulp-htmlmin');
var sourcemaps = require('gulp-sourcemaps');
var del = require('del');
var open = require('gulp-open');

var config = {
	production : !!util.env.production // !! = undefined -> false
}

console.log('Production: ' + !!util.env.production);

function handleError (err) {

	console.log(err);
	this.emit('end');
}

gulp.task('clean', function () {
  return del.sync([
    'build/**/*',
  ]);
});

gulp.task('statics',function(){
	gulp.src(['source/statics/**/*'])
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(gulp.dest('build/statics/'))
	.pipe(connect.reload());
});

gulp.task('templates',function(){
	gulp.src(['source/templates/*.html'])
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(nunjucks.compile({todo: 'todo'})) // todo: template daten...
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? htmlmin({
		collapseWhitespace: true
	}) : util.noop())
	.pipe(gulp.dest('build/'))
	.pipe(connect.reload());
});

gulp.task('styles',function(){
	gulp.src(['source/styles/*.{less,css}'])
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? util.noop() : sourcemaps.init())
	.pipe(less())
	.pipe(config.production ? util.noop() : sourcemaps.write())
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? cssnano() : util.noop())
	.pipe(gulp.dest('build/styles/'))
	.pipe(connect.reload());
});

gulp.task('scripts',function(){
	gulp.src(['source/scripts/*.js'])
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? util.noop() : sourcemaps.init())
	.pipe(include())
	.pipe(config.production ? util.noop() : sourcemaps.write())
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? uglify() : util.noop())
	.pipe(gulp.dest('build/scripts/'))
	.pipe(connect.reload());
});

gulp.task('images',function(){
	gulp.src(['source/images/**/*.{jpg,png,gif,svg}'])
	.pipe(plumber({
		handleError: handleError
	}))
	.pipe(config.production ? imagemin({
		progressive: true,
		svgoPlugins: [{removeViewBox: false}],
		use: [pngquant()]
	}) : util.noop())
	.pipe(gulp.dest('build/images/'))
	.pipe(connect.reload());
});

gulp.task('connect', function() {

	var cors = function (req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*');
		res.setHeader('Access-Control-Allow-Headers', '*');
		next();
	};

  connect.server({
    root: 'build',
    livereload: true,
    middleware: function(connect, opt) {
		return [cors]
	}
  });
});

gulp.task('openuri', function(){
	gulp.src('').pipe(open({uri: 'http://localhost:8080'}));
});

gulp.task(
	'default',
	['clean','templates','styles','scripts','images','statics','connect','openuri'],
	function(){
		
		watch('source/templates/**/*.html', function() {

			gulp.start('templates');
		});
		
		watch('source/styles/**/*.less', function() {

			gulp.start('styles');
		});
		
		watch('source/scripts/**/*.js', function() {

			gulp.start('scripts');
		});
		
		watch('source/images/**/*.{jpg,png,gif,svg}', function() {

			gulp.start('images');
		});
		
		watch('source/statics/**/*', function() {

			gulp.start('statics');
		});
	}
);