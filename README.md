# README #

This README documents whatever steps are necessary to get your application up and running.

### install gulp ###

* npm install --global gulp-cli

### setup process ###

* git clone https://bitbucket.org/kkutschera/gulp-build.git
* cd gulp-build/
* npm install
* gulp

* to minify: gulp --production

### features ###

* templating - nunjucks
* styles - less | cssnano | sourcemaps
* scripts - include | uglify
* images - imagemin
* autoreload

### help ###

* templating: https://mozilla.github.io/nunjucks/
* styling: http://lesscss.org/
* scripting: https://www.npmjs.com/package/gulp-include

### hints ###

* example to import less-files in less file use: @import "mod1/dep1";
* example to import js-files in js file use: //=require mod1/dep1.js
* example to import svg-files in template file use: {% include "svgs/test.svg" %}